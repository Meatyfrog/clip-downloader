const fs = require('fs');
const https = require('https');
const yargs = require('yargs');
const sanitize = require('sanitize-filename');

const argv = yargs.argv;

console.log(argv);

process.setMaxListeners(0);

if(argv.file){
    console.log(argv.file);
    fs.readFile(argv.file, 'utf8', function (err, data){
       if(err){
           return console.log(err);
       }

       var clips = JSON.parse(data);

        downloadClips(clips);

    });

}

async function downloadClips(clips){
    var start = 0;
    //command line support for skipping through clips.
    if(argv.start){
        start = argv.start;
    }
    for(var i = start; i < clips.length; i ++){
        var clip = clips[i];
        var title = clip.title;
        title = sanitize(title);
        try {
            var output = await downloadClip(title, clip.download_url);
            console.log(i + " out of " + clips.length + " downloaded " + output);
        }catch(e){
            console.log('Eating an error on ' + title);
        }
        if(argv.max){
            if(i > argv.max){
                break;
            }
        }

    }
}

async function downloadClip (title, download_url){
    return new Promise ((resolve, reject) => {
        try {
            var file = fs.createWriteStream("./downloaded/"+ title + ".mp4");
            var request = https.get(download_url, function (response, err) {
                if(err){
                    console.log(err);
                    reject(err);
                }
                response.pipe(file);
                resolve(title);
            });
        }catch(exception){
            console.log("Exception downloading " + title + " " + download_url + " i:" + i);
            console.log(exception);
            reject(exception);
        }
    });
}





